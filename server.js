'use strict'

const express = require('express')
const bodyParser = require('body-parser')
const port = process.env.PORT || 8000
const mongoose = require('mongoose')
const passport = require('passport')
const mLabUser = require('./server/credentials').mLabUser
const mLabPass = require('./server/credentials').mLabPass
const app = express()


mongoose.connect(`mongodb://${mLabUser}:${mLabPass}@ds049496.mlab.com:49496/dou`)
mongoose.connection.on('connected', () => {
    console.log('connected to DB')
})
mongoose.connection.on('error', () => {
    console.log('error connecting to DB')
})

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
require('./server/routes')(app)
app.use(express.static(__dirname + '/dist'))

app.listen(port, (err) => {
    if (err) {
        console.log(err)
    } else {
        console.log('\n' + 'Listening on port ' + '8000')
    }
})
