import React, { Component } from 'react'
import TripComponent from '../components/TripComponent'
import { connect } from 'react-redux'

class TripContainer extends Component {
    render() {
        return (
            <TripComponent {...this.props} />
        )
    }
}

const mapStateToProps = (state) => {
    let restLengthVal = state.rootReducer.placesReducer.restInfo.length !== 0,
    doLengthVal = state.rootReducer.placesReducer.doInfo.length !== 0,
    eventLengthVal = state.rootReducer.fbReducer.fbEvents.events.length !== 0,
    restLength = state.rootReducer.placesReducer.restInfo.length,
    doLength = state.rootReducer.placesReducer.doInfo.length
    return {
        destination: state.rootReducer.weatherReducer.name,
        forecast: state.rootReducer.weatherReducer.forecast.daily.data,
        restaurants: state.rootReducer.placesReducer.restaurants.results,
        dos: state.rootReducer.placesReducer.dos.results,
        fetchedRests: state.rootReducer.placesReducer.restInfo,
        fetchedDos: state.rootReducer.placesReducer.doInfo,
        selectedRestInfo: restLengthVal ? state.rootReducer.placesReducer.restInfo[restLength-1].data.result : null,
        selectedDoInfo: doLengthVal ? state.rootReducer.placesReducer.doInfo[doLength-1].data.result : null,
        myTrip: state.rootReducer.tripReducer,
        fbEvents: state.rootReducer.fbReducer.fbEvents.events,
        selectedEventInfo: eventLengthVal ? state.rootReducer.fbReducer.fbEvents.events[0] : null
    }
}
export default connect(mapStateToProps)(TripContainer)
