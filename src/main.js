import React from 'react'
import { render } from 'react-dom'
import { Router, browserHistory } from 'react-router'
import createLogger from 'redux-logger'
import thunk from 'redux-thunk'
import { Provider } from 'react-redux'
import { createStore, compose, applyMiddleware, combineReducers } from 'redux'
import { syncHistoryWithStore, routerReducer } from 'react-router-redux'
import { rootReducer } from './reducers/rootReducer'
import HomeContainer from './containers/HomeContainer'
import TripContainer from './containers/TripContainer'
import { Route, IndexRoute } from 'react-router'
import App from './App'

const logger = createLogger()
const store = createStore(
    combineReducers({
    rootReducer,
    routing: routerReducer
    }), applyMiddleware(thunk, logger))
const history = syncHistoryWithStore(browserHistory, store)
render(
    <Provider store={store}>
        <Router history={history}>
            <Route path='/' component={App}>
                <IndexRoute component={HomeContainer} />
                <Route path='/myTrip' component={TripContainer} />
            </Route>
        </Router>
    </Provider>
    , document.getElementById('app')
)