import React, { Component } from 'react'
import Header from './common/Header'

export default class App extends Component {
    render() {
        return (
            <div>
            <Header />
                <div id='ghost'></div>
                <div>
                    {this.props.children}
                </div>
            </div>
        )
    }
}