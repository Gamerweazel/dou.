import React, { Component } from 'react'

export default class Header extends Component {
    render () {
        return (
            <div>
                <div className="top-bar">
                    <div className="top-bar-left">
                        <a href='/'><h3 className="brand">City Live</h3></a>
                    </div>
                    <div className="top-bar-right">
                        <div className="menu">
                        <button type="button" className="success large button radius push-left">Get the App</button>
                        <button type="button" className="primary large button round push-left">Sign Up</button>
                        <button type="button" className="warning large button push-left">Login</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}



