import React, { Component } from 'react'

export default class Input extends Component {
    render() {
        return (
            <label>{this.props.name}
                <input type={this.props.type}
                    className={this.props.className} 
                    placeholder={this.props.placeholder}
                    onChange={this.props.onChange}
                    value={this.props.value}/>
            </label>
        )
    }
}