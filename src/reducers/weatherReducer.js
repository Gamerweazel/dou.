import * as types from '../actions/actions'

export function weatherReducer(state = {}, action) {
    switch (action.type) {
        case types.FETCH_WEATHER:
            return {...state, forecast: action.forecast, name: action.name }
            break
        default:
            return state
            break
    }
}