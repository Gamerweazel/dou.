import { combineReducers } from 'redux'
import { weatherReducer } from './weatherReducer'
import { placesReducer } from './placesReducer'
import { tripReducer } from './tripReducer'
import { fbReducer } from './fbReducer'

export const rootReducer = combineReducers({
    weatherReducer,
    placesReducer,
    fbReducer,
    tripReducer
})
