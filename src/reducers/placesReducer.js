import * as types from '../actions/actions'
import _ from 'lodash'

export function placesReducer(state = {
    dos: {results:[]},
    restaurants: {results:[]},
    restInfo: [],
    doInfo: []
}, action) {
    switch (action.type) {
        case types.FETCH_RESTS:
            return {...state,
                restaurants: action.restaurants
            }
            break
        case types.NO_RESTS:
            return state
            break
        case types.FETCH_DOS:
            return {...state,
                dos: action.dos
            }
            break
        case types.NO_DOS:
            return state
            break
        case types.FETCH_PLACE_INFO:
            let obj = {
                id: action.id,
                data: action.info
            }
            if (action.filter === 'restaurant') {
                let restInfoCopy = [...state.restInfo]
                restInfoCopy.push(obj)
                return {...state,
                    restInfo: restInfoCopy
                }
            } else {
                let doInfoCopy = [...state.doInfo]
                doInfoCopy.push(obj)
                return {...state,
                    doInfo: doInfoCopy
                }
            }
            break
        default:
            return state
            break
    }
}
