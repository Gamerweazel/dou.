import * as types from '../actions/actions'

export function fbReducer(state = {fbEvents: []}, action) {
    switch(action.type) {
        case types.FETCH_FACEBOOK_DOS:
            return {...state, fbEvents: action.events}
            break
        case types.NO_FACEBOOK_DOS:
            return state
        default:
            return state
            break
    }
}
