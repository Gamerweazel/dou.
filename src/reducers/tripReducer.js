import * as types from '../actions/actions'

export function tripReducer(state = { doing: [], eating: [] }, action) {
    switch (action.type) {
        case types.ADD_TRIP_ITEM:
            if (action.filter === 'do') {
                let doingCopy = [...state.doing]
                doingCopy.push(action.item)
                return {...state, doing: doingCopy }
            } else if (action.filter === 'restaurant') {
                let eatingCopy = [...state.eating]
                eatingCopy.push(action.item)
                return {...state, eating: eatingCopy }
            }
            break
        default:
            return state
            break
    }
}
