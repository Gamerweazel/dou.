const $ = require('jquery')

let domain

if (document.domain === 'localhost') {
  domain = 'http://' + document.domain + ':8000'
} else {
  domain = 'https://' + document.domain
}

export default function ajax(url, data, type) {
  let method = type || 'POST'
  if (data) {
    return $.ajax({
      url: domain +  url,
      datatype: 'json',
      contentType: 'application/json',
      type: method,
      responseType: 'json',
      data: JSON.stringify(data)

    })
  } else {
    return $.ajax({
      url: domain + url,
      datatype: 'json',
      contentType: 'application/json',
      type: method,
      responseType: 'json',
    })
  }
}
