import ajax from './ajax'

export function getWeather(lat, long) {
    const url = '/weather?lat=' + lat + '&long=' + long
    const data = false
    const type = 'GET'
    return ajax(url, data, type)
}

export function getRestaurants(lat, long) {
    const url = '/restaurants?lat=' + lat + '&long=' + long
    const data = false
    const type = 'GET'
    return ajax(url, data, type)
}

export function getPlaceInfo(id) {
    const url = '/placeInfo?id=' + id
    const data = false
    const type = 'GET'
    return ajax(url, data, type)
}

export function getPlacePhoto(ref) {
    const url = '/placePhoto?ref=' + ref
    const data = false
    const type = 'GET'
    return ajax(url, data, type)
}

export function getDos(lat, long) {
    const url = '/dos?lat=' + lat + '&long=' + long
    const data = false
    const type = 'GET'
    return ajax(url, data, type)
}

export function getFacebook(lat, long) {
    const url = '/facebook?lat=' + lat + '&long=' + long
    const data = false
    const type = 'GET'
    return ajax(url, data, type)
}
