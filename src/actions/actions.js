import { browserHistory } from 'react-router'
import * as API from '../requests/api'

export const ADD_TRIP_ITEM = 'ADD_TRIP_ITEM'
export const FETCH_PLACE_INFO = 'FETCH_PLACE_INFO'
export const FETCH_RESTS = 'FETCH_RESTS'
export const NO_RESTS = 'NO_RESTS'
export const FETCH_DOS = 'FETCH_DOS'
export const NO_DOS = 'NO_DOS'
export const FETCH_FACEBOOK_DOS = 'FETCH_FACEBOOK_DOS'
export const NO_FACEBOOK_DOS = 'NO_FACEBOOK_DOS'
export const FETCH_WEATHER = 'FETCH_WEATHER'
export const DEST_CITY = 'DEST_CITY'
export const FETCH_AIRCODES = 'FETCH_AIRCODES'
export const FETCH_FLIGHTS = 'FETCH_FLIGHTS'

export function fetchWeather(lat, long, name) {
    return dispatch => {
        const weatherPromise = API.getWeather(lat, long)
            .then(data => {
                dispatch({
                    type: FETCH_WEATHER,
                    forecast: data,
                    name
                })
            })
            .fail((xhr, status, error) => {
                console.log('error getting weather data')
            })
            .always(() => dispatch(fetchRestaurants(lat, long)))
    }
}

export function fetchRestaurants(lat, long) {
    return dispatch => {
        const restPromise = API.getRestaurants(lat, long)
            .then(data => {
                if (data.status === 'OK') {
                    dispatch({
                        type: FETCH_RESTS,
                        restaurants: data
                    })
                    dispatch(fetchPlaceInfo(data.results[0].place_id,
                      'restaurant'))
                } else if (data.status === 'ZERO_RESULTS') {
                    dispatch({
                        type: NO_RESTS
                    })
                }
            })
            .fail((xhr, status, error) => {
                console.log('error fetching list of restaruants')
            })
            .always(() => dispatch(fetchDos(lat, long)))
    }
}

export function fetchDos(lat, long) {
    return dispatch => {
        const dosPromise = API.getDos(lat, long)
            .then(data => {
                if (data.status === 'OK') {
                    dispatch({
                        type: FETCH_DOS,
                        dos: data
                    })
                    dispatch(fetchPlaceInfo(data.results[0].place_id, 'do'))
                } else if (data.status === 'ZERO_RESULTS') {
                    dispatch({
                        type: NO_DOS
                    })
                }
            })
            .fail((xhr, status, error) => {
                console.log('error fetching list of dos')
            })
            .then(() => dispatch(fetchFBEvnts(lat, long)))
    }
}

export function fetchPlaceInfo(id, filter) {
    return dispatch => {
        const placePromise = API.getPlaceInfo(id)
            .then(data => {
                dispatch({
                    type: FETCH_PLACE_INFO,
                    id: id,
                    info: data,
                    filter
                })
            })
            .fail((xhr, status, error) => {
                console.log('error fetching place info')
            })
    }
}

export function fetchFBEvnts(lat, long) {
    return (dispatch, getState) => {
        const eventPromise = API.getFacebook(lat, long)
            .then(data => {
                if (data.events) {
                    dispatch({
                        type: FETCH_FACEBOOK_DOS,
                        events: data
                    })
                } else {
                    dispatch({
                        type: NO_FACEBOOK_DOS
                    })
                }
            })
            .fail((xhr, status, error) => {
                console.log('error fetching facebook events')
            })
            .then(() => {
                if (getState().routing.locationBeforeTransitions.pathname != '/myTrip')
                    browserHistory.push('/myTrip')
            })
    }
}

export function addTripItem(item, filter) {
    return dispatch =>
        dispatch({
            type: ADD_TRIP_ITEM,
            item,
            filter
        })
}
