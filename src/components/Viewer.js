import React, { Component } from 'react'
import { photoUrl } from '../requests/photos'
const questionMark = require('../../dist/resources/question-mark.jpg')

export default class Viewer extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        const { place, type } = this.props
        let name, imgSrc, address, contact, desc, site, hours, reviews, atten
        if (!place) return null
        else if (type === 'google') {
             imgSrc = place.photos ? photoUrl.replace('photoreference=',
                 'photoreference=' + place.photos[0].photo_reference) : questionMark
             address = place.formatted_address ? place.formatted_address
                 : null
             contact = place.formatted_phone_number ?
                 place.formatted_phone_number : null
             site = place.website ? (<a href={place.website}>Website</a>)
                 : null
             hours = place.opening_hours ? place.opening_hours.weekday_text.map(hour => {
                 return (<li key={Math.random()}>{hour}</li>)}) : null
             reviews = place.reviews ? place.reviews.map(review => {
                 return (<li key={Math.random()}>{review.text}</li>)}) : null
             } else if (type === 'fb') {
                 imgSrc = place.coverPicture ? place.coverPicture : questionMark
                 address = place.venue ? place.venue.location.street + place.venue.location.zip : null
                 contact = place.venue.emails ? place.venue.emails[0] : null
                 hours = place.startTime ? place.startTime : null
                 desc = place.description ? place.description : null
                 atten = place.stats ? place.stats.attending : null
             }
             name = place.name ? place.name : 'No name listed'
         return (
             <div className='large-6 medium-6 small-6 columns text-centered'>
                 <button type="button" className="success button"
                     onClick={this.props.onClick}>Add</button>
                 <div className='media-object'>
                     <div className='media-object-section'>
                         <div className='thumbnail'>
                             <img src={imgSrc} width={300} height={300}/>
                             <div className='media-object-section'>
                                 <h3>{name}</h3>
                                 <h4>{address}</h4>
                                 <h4>{contact}</h4>
                                 <ul className='no-bullet'>
                                     {hours}
                                 </ul>
                                 <p>{site || desc}</p>
                                 <ul className='no-bullet'>
                                     {reviews || atten}
                                 </ul>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
             )
    }
}
