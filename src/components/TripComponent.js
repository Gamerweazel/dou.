import React, { Component } from 'react'
import Input from '../common/Input'
import Weather from './Weather'
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs'
import Viewer from './Viewer'
import List from './List'
import _ from 'lodash'
import { fetchPlaceInfo, addTripItem } from '../actions/actions'

export default class TripComponent extends Component {
    constructor(props) {
        super(props)
        const { selectedDoInfo, selectedRestInfo, selectedEventInfo } = this.props
        this.handleTabSelect = this.handleTabSelect.bind(this)
        this.addToMyTrip = this.addToMyTrip.bind(this)
        this.onItemClick = this.onItemClick.bind(this)
        this.state = {
            selectedDo: selectedDoInfo,
            selectedRest: selectedRestInfo,
            selectedEvent: selectedEventInfo,
            selectedIndex: 0,
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.selectedRestInfo != this.state.selectedRest) {
            this.setState({
                selectedRest: nextProps.selectedRestInfo,
                selectedDo: this.state.selectedDo,
                selectedIndex: this.state.selectedIndex,
                selectedEvent: this.state.selectedEvent
            })
        } else if (nextProps.selectedDoInfo != this.state.selectedDo) {
            this.setState({
                selectedRest: this.state.selectedRest,
                selectedDo: nextProps.selectedDoInfo,
                selectedIndex: this.state.selectedIndex,
                selectedEvent: this.state.selectedEvent
            })
        }
    }

    addToMyTrip() {
        const { dispatch } = this.props
        switch (this.state.selectedIndex) {
            case 1:
                if (!(_.find(this.props.myTrip.doing, { name: this.state.selectedDo.name })))
                    dispatch(addTripItem(this.state.selectedDo, 'do'))
                break
            case 3:
                if (!(_.find(this.props.myTrip.eating, { name: this.state.selectedRest.name })))
                    dispatch(addTripItem(this.state.selectedRest, 'restaurant'))
                break
        }
    }

    onItemClick(id, filter) {
        const { dispatch, fetchedRests, fetchedDos, fbEvents } = this.props
        let stateObj = {...this.state }, list, key, type, index;
        switch (this.state.selectedIndex) {
            case 1:
                list = fetchedDos
                key = 'selectedDo'
                type = 'do'
                break
            case 3:
                list = fetchedRests
                key = 'selectedRest'
                type = 'restaurant'
                break
            case 4:
            console.log('firing', filter);
                list = fbEvents
                key = 'selectedEvent'
                break
        }
        index = _.findIndex(list, { id: id })
        if (index != -1) {
            if (filter === 'google') {
                stateObj[key] = list[index].data.result
            } else {
                console.log('found');
                stateObj[key] = list[index]
            }
            this.setState(stateObj)
        } else {
            if (filter === 'google')
                dispatch(fetchPlaceInfo(id, type))
        }
    }

    handleTabSelect(index) {
        this.setState({
            selectedIndex: index
        })
    }

    render() {
        return (
            <div>
                <div className='row'>
                    <div className='small-12 columns text-center'>
                        <h1 className='title'>{this.props.destination || 'Nothing'}</h1>
                    </div>
                </div>
                <div className='row'>
                    <div className='large-12 large-centered small-12 small-centered columns'>
                        <Weather data={this.props.forecast} />
                    </div>
                </div>
                <div className='row subtleRow'>
                    <div className='large-12 medium-12 small-12 columns large-centered text-center'>
                        <Tabs
                            onSelect={this.handleTabSelect}
                            selectedIndex={this.state.selectedIndex}>
                            <TabList>
                                <Tab>My Trip</Tab>
                                <Tab>Things To Do</Tab>
                                <Tab>Food & Drink</Tab>
                                <Tab>Need To Know</Tab>
                                <Tab>Getting Around</Tab>
                                <Tab>Tools</Tab>
                            </TabList>
                            <TabPanel>
                                <h2>My Trip</h2>
                                <div className='row'>
                                    <List dataSource={this.props.myTrip.doing}
                                        onClick={() => {console.log('click')}}
                                        type='google'/>
                                    <List dataSource={this.props.myTrip.eating}
                                        onClick={() => {console.log('click')}}
                                        type='google'/>
                                </div>
                            </TabPanel>
                            <TabPanel>
                                <div className='row'>
                                    <List dataSource={this.props.dos}
                                        onClick={this.onItemClick}
                                        type='google'/>
                                    <Viewer place={this.state.selectedDo}
                                        onClick={this.addToMyTrip}
                                        type='google'/>
                                </div>
                            </TabPanel>
                            <TabPanel>
                                <h2>Seeing</h2>
                            </TabPanel>
                            <TabPanel>
                                <div className='row'>
                                    <List dataSource={this.props.restaurants}
                                        onClick={this.onItemClick}
                                        type='google'/>
                                    <Viewer place={this.state.selectedRest}
                                        onClick={this.addToMyTrip}
                                        type='google'/>
                                </div>
                            </TabPanel>
                            <TabPanel>
                                <h2>Events</h2>
                                    <List dataSource={this.props.fbEvents}
                                        onClick={this.onItemClick}
                                        type='fb'/>
                                    <Viewer place={this.state.selectedEvent}
                                        onClick={this.addToMyTrip}
                                        type='fb'/>
                            </TabPanel>
                        </Tabs>
                    </div>
                </div>
            </div>
        )
    }
}
