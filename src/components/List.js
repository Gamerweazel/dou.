import React, { Component } from 'react'

export default class List extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        const { dataSource, type } = this.props
        let items, id
        if (dataSource.length === 0) return null
        else {
            if (type === 'google') {id = 'place_id'}
            else {id = 'id'}
        items = dataSource.map(item => {
            return (<li key={item[id]}
                onClick={this.props.onClick.bind(this, item[id], type)}>{item.name}</li>)
        })
        return (
            <div className='large-6 medium-6 small-6 columns text-centered'>
                <ul className='no-bullet'>{items}</ul>
            </div>
            )
        }
    }
}
