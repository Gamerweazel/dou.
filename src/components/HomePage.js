import React, { Component } from 'react'
import Input from '../common/Input'
import Geosuggest from 'react-geosuggest'
import { fetchWeather, fetchFBEvnts } from '../actions/actions'
import { Link, browserHistory } from 'react-router'

export default class HomePage extends Component {
    constructor(props) {
        super(props)
        this.onClick = this.onClick.bind(this)
        this.onChange = this.onChange.bind(this)
        this.onChangeDate = this.onChangeDate.bind(this)
        this.state = {
            fromLocation: 'Denver',
            toLocation: 'Berlin'
        }
    }

    onClick() {
        const { dispatch } = this.props
        const obj = {...this.state }
        dispatch(fetchWeather(obj.toLocation.lat, obj.toLocation.lng, obj.toName))
    }

    onChange(selected, bool) {
        if (bool === true)
            this.setState({
                fromLocation: selected.location,
                fromName: selected.label
            })
        else
            this.setState({
                toLocation: selected.location,
                toName: selected.label
            })
    }

    onChangeDate(e, bool) {
        if (bool === true)
            this.setState({ fromDate: e.target.value })
        else
            this.setState({ toDate: e.target.value })
    }

    render() {
        return (
            <div>
                <div className='row middle'>
                    <div className='large-12 medium-12 small-12 columns large-centered medium-centered small-centered'>
                        <h1 className='text-heading text-center'>EXPLORE THE WORLD</h1>
                    </div>
                    <div className='large-3 medium-3 small-3 columns large-centered medium-centered small-centered'></div>
                    <div className='large-6 medium-6 small-6 columns large-centered medium-centered small-centered'>
                        <Geosuggest
                            placeholder='Where do you want to go?'
                            types={['(cities)']}
                            onSuggestSelect={(selected) => this.onChange(selected, false) }
                            autoFocus={false} />
                    </div>
                    <div className='large-3 medium-3 small-3 columns large-centered medium-centered small-centered'></div>
                </div>
                <br />
                <div className='row'>
                    <div className='large-5 medium-12 small-12 columns large-centered text-center'>
                        <button type="button" className="warning large button"
                            onClick={() => this.onClick() }>Lets Go!</button>
                    </div>
                </div>
            </div>
        )
    }
}
