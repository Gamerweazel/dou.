import React, { Component } from 'react'

const days = ['Sunday', 'Monday', 'Tuesday',
    'Wednesday', 'Thursday', 'Friday', 'Saturday']

export default class Weather extends Component {
    constructor(props) {
        super(props)
        this.assignImg = this.assignImg.bind(this)
    }

    assignImg(weather) {
        let clear = require('../../dist/resources/weather/clear.png')
        let rain = require('../../dist/resources/weather/rain01.png')
        let clouds = require('../../dist/resources/weather/cloudy.png')
        let snow = require('../../dist/resources/weather/snow.png')
        switch (weather) {
            case 'rain':
            return rain
            case 'clear-day':
            return clear
            case 'Snow':
            return snow
            default:
            return clear
        }
    }

    render() {
        const { data } = this.props
        let weatherItems = data.map(item => {
            let d = new Date(item.time * 1000)
            return (
                <li key={Math.random()}>
                    <p className='title'>{days[d.getDay()]}</p>
                    <img src={this.assignImg(item.icon) } />
                    <p className='title'>{Math.floor(item.temperatureMax) }</p>
                </li>
            )
        }) || 'No weather to display'

        return (
            <div className='large-12 medium-12 small-6 columns'>
                <ul className='menu'>
                    {weatherItems}
                </ul>
            </div>
        )
    }
}
