'use strict'

const webpack = require('webpack')
const path = require('path')

let plugins = [
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery"
        }),
        new webpack.NoErrorsPlugin(),
//         new webpack.DefinePlugin({
//     'process.env.GOOGLE_KEY': JSON.stringify(process.env.GOOGLE_KEY)
// })
    ]

const config = {
    entry: [
        './dist/foundation-6.2.3-complete/js/vendor/what-input.js',
        './dist/foundation-6.2.3-complete/js/vendor/foundation.js',
        './dist/foundation-6.2.3-complete/js/app.js',
        './src/main.js',
    ],
    output: {
        path: path.join(__dirname, 'dist'),
        filename: 'bundle.js',
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                loaders: ['babel'],
                exclude: /node_modules/
            },
            { test: /\.(png|jpg)$/, loader: 'url-loader?limit=8192' }
        ]
    },
    plugins: plugins

}

module.exports = config
