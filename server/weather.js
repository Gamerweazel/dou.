'use strict'

const weatherKey = require('./credentials').weatherKey

const weatherUrl = 'https://api.darksky.net/forecast/' + weatherKey + '/'

module.exports = weatherUrl