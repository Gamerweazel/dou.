'use strict'

const fbID = process.env.FB_ID
const fbSecret = process.env.FB_SECRET
const googleKey = process.env.GOOGLE_KEY
const aeroKey = process.env.AERO_KEY
const weatherKey = process.env.WEATHER_KEY
const mLabUser = process.env.MLAB_USER
const mLabPass = process.env.MLAB_PASS

module.exports = {
    fbID,
    fbSecret,
    googleKey,
    aeroKey,
    weatherKey,
    mLabUser,
    mLabPass
}
