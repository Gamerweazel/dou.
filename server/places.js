'use strict'

const googleKey = require('./credentials').googleKey

const placesRestUrl = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=&radius=500&type=restaurant&rankby=prominence&key=' + googleKey
const placesDoUrl = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=&radius=500&types=amusement_park|aquarium|art_gallery|book_store|casino|night_club|clothing_store|shopping_mall|spa|movie_theater|bowling_alley&rankby=prominence&key=' + googleKey
const placeInfoUrl = 'https://maps.googleapis.com/maps/api/place/details/json?placeid=&key=' + googleKey
const placePhotoUrl = 'https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=&key=' + googleKey

module.exports = {
    placesRestUrl,
    placesDoUrl,
    placeInfoUrl,
    placePhotoUrl
}
