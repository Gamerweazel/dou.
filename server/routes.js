'use strict'

const path = require('path')
const request = require('request')
const weatherUrl = require('./weather')
const placesRestUrl = require('./places').placesRestUrl
const placesDoUrl = require('./places').placesDoUrl
const placeInfoUrl = require('./places').placeInfoUrl
const placePhotoUrl = require('./places').placePhotoUrl
const EventSearch = require('facebook-events-by-location-core')
const fbID = require('./credentials').fbID
const fbSecret = require('./credentials').fbSecret
const corsProxy = 'http://cors-anywhere.herokuapp.com/'

module.exports = (app) => {
        app.get('/', (req, res) => {
            res.sendFile(path.join(__dirname, '../dist/index.html'))
        })

        app.get('/myTrip', (req, res) => {
          res.sendFile(path.join(__dirname, '../dist/index.html'))
        })

        app.get('/weather*', (req, res) => {
            console.log('weather')
            const target = corsProxy + weatherUrl + req.query.lat + ',' + req.query.long
            req.pipe(request(target)).pipe(res)
        })

        app.get('/restaurants*', (req, res) => {
            console.log('rests')
            const target = corsProxy + placesRestUrl.replace('location=', 'location=' + req.query.lat + ',' + req.query.long)
            req.pipe(request(target)).pipe(res)
        })

        app.get('/dos*', (req, res) => {
            console.log('dos')
            const target = corsProxy + placesDoUrl.replace('location=', 'location=' + req.query.lat + ',' + req.query.long)
            console.log(target)
            req.pipe(request(target)).pipe(res)
        })

        app.get('/placeInfo*', (req, res) => {
            console.log('place')
            const target = corsProxy + placeInfoUrl.replace('placeid=', 'placeid=' + req.query.id)
            req.pipe(request(target)).pipe(res)
        })

        app.get('/facebook*', (req, res) => {
              console.log('facebook')
              const es = new EventSearch({
                  lat: req.query.lat,
                  lng: req.query.long,
                  accessToken: fbID + '|' + fbSecret,
                  distance: 2500
              })
              es.search().then(function(events) {
                  res.json(events)
              }).catch(function(error) {
                  console.error(JSON.stringify(error));
              });
          })
        }
