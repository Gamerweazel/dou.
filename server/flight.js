'use strict'

const googleKey = require('./credentials').googleKey
const body = {
    "request": {
        "slice": [
            {
                "origin": "",
                "destination": "",
                "date": ""
            }
        ],
        "passengers": {
            "adultCount": 1,
            "infantInLapCount": 0,
            "infantInSeatCount": 0,
            "childCount": 0,
            "seniorCount": 0
        },
        "solutions": 20,
        "refundable": false
    }
}
const flightUrl = 'https://www.googleapis.com/qpxExpress/v1/trips/search?key=' + googleKey
