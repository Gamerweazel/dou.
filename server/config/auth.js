let config = {}

module.exports = {
    facebookAuth: {
        clientID: config.facebookClientID,
        clientSecret: config.facebookClientSecret,
        callbackURL: config.domain + 'auth/facebook/callback',
        profileFields: ['email', 'first_name', 'last_name', 'picture.type(large)']
    },
    googleAuth: {
        clientID: config.googleClientID,
        clientSecret: config.googleClientSecret,
        callbackURL: config.domain + 'auth/google/callback',
        profileFields: ['email', 'first_name', 'last_name', 'photos']
    }
}
