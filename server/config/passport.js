'use strict'

const FacebookStrategy = require('passport-facebook').Strategy
const GoogleStrategy = require('passport-google-oauth').OAuth2Strategy
const User = require('../user/model')

module.exports = (passport) => {
    passport.use(new FacebookStrategy({
                clientID: auth.facebookAuth.clientID,
                clientSecret: auth.facebookAuth.clientSecret,
                callbackURL: auth.facebookAuth.callbackURL,
                profileFields: auth.facebookAuth.profileFields
            },
            (accessToken, refreshToken, profile, done) => {
                process.nextTick(() => {
                    User.findOne({
                        'facebook.id': profile.id
                    }, function(err, user) {
                        if (err)
                            return done(err)
                        if (user)
                            return done(null, user)
                        else
                            let newUser = new User()
                        newUser.facebook.id = profile.id
                        newUser.facebook.token = accessToken
                        newUser.facebook.name = profile.name.givenName + ' ' + profile.name.familyName
                        newUser.facebook.email = profile.emails[0].value
                        newUser.facebook.picture = "https://graph.facebook.com/" + profile.username + "/picture" + "?width=200&height=200" + "&access_token=" + accessToken

                        newUser.save(function() {
                            if (err)
                                throw err
                            return done(newUser)
                        })
                    })
                })
                return done(null, profile)
            }))
        // Use the GoogleStrategy within Passport.
    passport.use(new GoogleStrategy({
            clientID: auth.googleAuth.clientID,
            clientSecret: auth.googleAuth.clientSecret,
            callbackURL: auth.googleAuth.callbackURL,
            profileFields: auth.googleAuth.profileFields
        },
        function(accessToken, refreshToken, profile, done) {
            process.nextTick(function() {
                User.findOne({
                    'google.id': profile.id
                }, function(err, user) {
                    if (err)
                        return done(err)
                    if (user)
                        return done(null, user)
                    else
                        let newUser = new User()
                    newUser.google.id = profile.id
                    newUser.google.token = accessToken
                    newUser.google.name = profile.displayName
                    newUser.google.email = profile.emails[0].value
                    newUser.google.picture = profile.photos[0].value

                    newUser.save(function() {
                        if (err)
                            throw err
                        return done(newUser)
                    })
                })
            })
            return done(null, profile)
        }))
    passport.serializeUser(function(user, done) {
        done(null, user)
    })
    passport.deserializeUser(function(obj, done) {
        done(null, obj)
    })
}
